package com.solomia.DAO;

import com.solomia.connector.DBManager;
import com.solomia.model.Diagnosis;
import com.solomia.model.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DiagnosisDAO implements DAO<Diagnosis> {
    private static Logger logger = LogManager.getLogger(DiagnosisDAO.class);
    private static final String CREATE = "INSERT INTO diagnosis VALUES (?, ?)";
    private static final String UPDATE = "UPDATE doctor SET doctor_name=?, doctor_email=?, doctor_position=? WHERE doctor_id=?";
    private static final String DELETE = "DELETE FROM doctor WHERE doctor_id=?";
    private static final String READ_ALL = "SELECT * FROM diagnosis";
    private static final String READ_BY_ID = "SELECT * FROM diagnosis where diagnosis_id=?";


    @Override
    public void create(Diagnosis entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getDiagnosisId());
            ps.setString(2,entity.getDiagnosisName());
            ps.executeUpdate();
        }
    }

    @Override
    public void update(Diagnosis entity) throws SQLException {
        System.out.println("Do not work, yet");
//        Connection connection = DBManager.getConnection();
//        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
//            ps.setInt(1,entity.getDoctorId());
//            ps.setString(2,entity.getDoctorName());
//            ps.executeUpdate();
//        }
    }

    @Override
    public void delete(int id) throws SQLException {
        System.out.println("Do not work, yet");
//        Connection connection = DBManager.getConnection();
//        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
//            ps.setInt(1,id);
//            ps.executeUpdate();
//        }
    }

    public List<Diagnosis> readById(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(READ_BY_ID);
        ps.setInt(1,id);
        ResultSet resultSet = ps.executeQuery();
        List<Diagnosis> diagnosisList = new ArrayList<>();
        while (resultSet.next()) {
            diagnosisList.add(new Diagnosis(resultSet.getInt("diagnosis_id"),
                    resultSet.getString("diagnos_name")));
        }
        return diagnosisList;
    }

    @Override
    public void read() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(READ_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<Diagnosis> realtorList = new ArrayList<>();
        List<Diagnosis> diagnosisList = new ArrayList<>();
        while (resultSet.next()) {
            diagnosisList.add(new Diagnosis(resultSet.getInt("diagnosis_id"),
                    resultSet.getString("diagnos_name")));
        }
        logger.info(diagnosisList);
    }
}
