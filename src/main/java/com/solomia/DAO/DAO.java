package com.solomia.DAO;

import com.solomia.model.Message;

import java.sql.SQLException;
import java.util.List;

public interface DAO<T> {
    void create(T t) throws SQLException;
    void read() throws SQLException;
    void update(T t) throws SQLException;
    void delete(int id) throws SQLException;
}
