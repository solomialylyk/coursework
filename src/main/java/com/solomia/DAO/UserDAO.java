package com.solomia.DAO;

import com.solomia.connector.DBManager;
import com.solomia.model.Message;
import com.solomia.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements DAO<User> {
    private static Logger logger = LogManager.getLogger(UserDAO.class);
    private static final String CREATE = "INSERT INTO user VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE client SET client_name=?, client_email=? WHERE client_id=?";
    private static final String DELETE = "DELETE FROM client WHERE client_id=?";
    private static final String READ_ALL = "SELECT * FROM user";
    private static final String READ_BY_ID = "SELECT * FROM user where user_id=?";
    private static final String READ_PASSWORD = "SELECT user_password FROM user where user_id=?";

    @Override
    public void create(User entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getUserId());
            ps.setString(2,entity.getUserName());
            ps.setString(3,entity.getUserStatus());
            ps.setString(4,entity.getPassword());
            ps.executeUpdate();
        }
    }

    @Override
    public void update(User entity) throws SQLException {
        System.out.println("Do no work, yet");
//        Connection connection = DBManager.getConnection();
//        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
//            ps.setInt(1,entity.getClientId());
//            ps.setString(2,entity.getClientName());
//            ps.setString(3,entity.getClientEmail());
//            ps.executeUpdate();
//        }
    }

    @Override
    public void delete(int id) throws SQLException {
        System.out.println("Do no work, yet");
//        Connection connection = DBManager.getConnection();
//        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
//            ps.setInt(1,id);
//            ps.executeUpdate();
//        }
    }

    public List<User> readById(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(READ_BY_ID);
        ps.setInt(1,id);
        ResultSet resultSet = ps.executeQuery();
        List<User> userList = new ArrayList<>();
        while (resultSet.next()) {
            userList.add(new User(resultSet.getInt("user_id"),
                    resultSet.getString("user_name"), resultSet.getString("user_position"),
                    resultSet.getString("user_password")));
        }
        return userList;
    }

    public String readPassword(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(READ_PASSWORD);
        ps.setInt(1,id);
        ResultSet resultSet = ps.executeQuery();
        if(resultSet.next()){
            return resultSet.getString("user_password");
        }
        return "Something go wrong";
    }


    @Override
    public void read() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(READ_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<User> userList = new ArrayList<>();
        while (resultSet.next()) {
            userList.add(new User(resultSet.getInt("user_id"),
                    resultSet.getString("user_name"), resultSet.getString("user_position"),
                    resultSet.getString("user_password")));
        }
        logger.info(userList);
    }
}

