package com.solomia.DAO;

import com.solomia.connector.DBManager;
import com.solomia.model.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageDAO implements DAO<Message> {
    private static Logger logger = LogManager.getLogger(MessageDAO.class);
    private static final String CREATE = "INSERT INTO message VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE message SET status_read = true WHERE message_id=?";
    private static final String DELETE = "DELETE FROM message WHERE message_id=?";
    //private static final String DELETEBYCONTRACT = "DELETE FROM payment WHERE contract_id=?";
    private static final String READ_ALL = "SELECT * FROM message";
    private static final String READ_BY_ID = "SELECT * FROM message where message_id=?";
    private static final String FIND_BY_STATUS = "SELECT message_id from message where recipient_id= ? and status_read =false";

    @Override
    public void create(Message entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getMessageId());
            ps.setString(6,entity.getTextMessage());
            ps.setInt(3,entity.getReceiveFrom());
            ps.setInt(4,entity.getSendTo());
            ps.setInt(5,entity.getDiagnosisId());
            ps.setBoolean(2, entity.getStatus());
            ps.executeUpdate();
        }
    }

    public void update(Message entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getMessageId());
            ps.setString(2,entity.getTextMessage());
            ps.setInt(3,entity.getReceiveFrom());
            ps.setInt(4,entity.getSendTo());
            ps.setBoolean(5, entity.getStatus());
            ps.executeUpdate();
        }
    }

    public void changeStatus(int message_id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(UPDATE);
        ps.setInt(1, message_id);
        ps.executeUpdate();
    }

    @Override
    public void delete(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    @Override
    public void read() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(READ_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<Message> messageList = new ArrayList<>();
        while (resultSet.next()) {
            messageList.add(new Message(resultSet.getInt("message_id"), resultSet.getString("message_text"),
                                 resultSet.getInt("recipient_id"),
                                resultSet.getInt("sender_id"), resultSet.getInt("diagnosis_id"), resultSet.getBoolean("status_read")));
        }
        logger.info(messageList);
    }

    public List<Message> readById(int message_id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(READ_BY_ID);
        ps.setInt(1,message_id);
        ResultSet resultSet = ps.executeQuery();
        List<Message> messageList = new ArrayList<>();
        while (resultSet.next()) {
            messageList.add(new Message(resultSet.getInt("message_id"), resultSet.getString("message_text"),
                    resultSet.getInt("recipient_id"),
                    resultSet.getInt("sender_id"), resultSet.getInt("diagnosis_id"), resultSet.getBoolean("status_read")));
        }
        return messageList;
    }

    public List<Integer> findUnread (int recipient_id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(FIND_BY_STATUS);
        ps.setInt(1, recipient_id);
        ResultSet resultSet = ps.executeQuery();
        List<Integer> integerList = new ArrayList<>();
        while (resultSet.next()) {
            integerList.add(resultSet.getInt("message_id"));
        }
        return integerList;
    }
}
