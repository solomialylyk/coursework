package com.solomia.view;

import com.solomia.service.Authorization;
import com.solomia.service.MessageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger= (Logger) LogManager.getLogger(MyView.class);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private MessageService messageService = new MessageService();

    public MyView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Read message by id");
        menu.put("2", "  2 - Find unread message");
        menu.put("3", "  3 - Send massage");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton3()  {
        logger.info("Input the sender's id");
        int temp = input.nextInt();
        messageService.send(temp);
    }

    private void pressButton2()  {
        logger.info("Input the recipient's id: ");
        int temp = input.nextInt();
        messageService.findUnread(temp);
    }


    private void pressButton1() {
        logger.info("Input message id: ");
        int temp = input.nextInt();
        messageService.readById(temp);
    }

    public void show() throws SQLException {
        Authorization authorization= new Authorization();
        authorization.authorize();
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).printGoods();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

}