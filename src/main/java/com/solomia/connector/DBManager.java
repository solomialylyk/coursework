package com.solomia.connector;

import com.solomia.pathfinder.PathFinder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
    private static Logger logger = LogManager.getLogger(DBManager.class);
    private static Connection connection = null;

    private DBManager() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                PathFinder path = new PathFinder();
                connection = DriverManager.getConnection(path.getPathToDB(), path.getUserName(), path.getUserPassword());
            } catch (SQLException e) {
                logger.error("SQLException: " + e.getMessage());
                logger.error("SQLState: " + e.getSQLState());
                logger.error("VendorError: " + e.getErrorCode());
            }
        }
        try {
            if(!connection.isClosed()) {
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return connection;
    }
}
