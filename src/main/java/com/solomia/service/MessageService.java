package com.solomia.service;

import com.solomia.DAO.MessageDAO;
import com.solomia.model.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class MessageService {
    private static Logger logger = LogManager.getLogger(MessageService.class);
    private Scanner scanner = new Scanner(System.in);
    private MessageDAO message= new MessageDAO();
    private static int countMessage =4;

    public void send(int sender_id) {
        scanner.useLocale(new Locale("ru"));
        logger.info("Input your message");
        String text = scanner.nextLine();
        logger.info("Input id of recipient");
        int recipient_id = scanner.nextInt();
        System.out.println("recipient_id ="+ recipient_id);
        countMessage++;
        try {
            message.create(new Message(countMessage, text,  recipient_id, sender_id, 8, Boolean.FALSE));
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    public void readById(int message_id) {
        try {
            List<Message> messageList = message.readById(message_id);
            if (messageList.isEmpty()) {
                logger.info("There is no such sms");
            } else {
                logger.info(messageList);
                changeStatus(message_id);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    private void changeStatus(int message_id) {
        try {
            message.changeStatus(message_id);
        } catch (SQLException e) {
            logger.error(e);
        }
    }


    public void findUnread(int recipient_id) {
        try {
            List<Integer> integerList = message.findUnread(recipient_id);
            if(integerList.isEmpty()) {
                logger.info("You have not unread message");
            } else {
                logger.info("Unread message :"+ integerList);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }
}
