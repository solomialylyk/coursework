package com.solomia.service;

import com.solomia.DAO.UserDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.Scanner;

public class Authorization {
    private static Logger logger = LogManager.getLogger(Authorization.class);

    public void authorize() throws SQLException {
        UserDAO daoUser = new UserDAO();
        Scanner scanner = new Scanner(System.in);
        logger.info("Input your id");
        Integer id = scanner.nextInt();
        int allowedAttempts = 3;
        boolean entring = false;
        while (allowedAttempts > 0){
            allowedAttempts --;
            logger.info("Input your password");
            String password = scanner.next();
            if (!password.equals(daoUser.readPassword(id))) {
                logger.error("Incorrect password!");
            } else {
                logger.info("Password is correct. You are in system!");
                entring = true;
                break;
            }
        }
        if (entring == false) {
            System.exit(0);
        }
    }
}
