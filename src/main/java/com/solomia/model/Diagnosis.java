package com.solomia.model;

public class Diagnosis {
    private Integer diagnosisId;
    private String diagnosisName;

    public Diagnosis() {
    }

    public Diagnosis(Integer diagnosisId, String diagnosisName) {
        this.diagnosisId = diagnosisId;
        this.diagnosisName = diagnosisName;
    }

    public Integer getDiagnosisId() {
        return diagnosisId;
    }

    public void setDiagnosisId(Integer diagnosisId) {
        this.diagnosisId = diagnosisId;
    }

    public String getDiagnosisName() {
        return diagnosisName;
    }

    public void setDiagnosisName(String diagnosisName) {
        this.diagnosisName = diagnosisName;
    }

    @Override
    public String toString() {
        return "Diagnosis{" +
                "diagnosisId=" + diagnosisId +
                ", diagnosisName='" + diagnosisName + '\'' +
                '}';
    }
}
