package com.solomia.model;

public class User {
    private Integer userId;
    private String userName;
    private String userStatus;
    private String password;

    public User(Integer userId, String userName, String userStatus, String password) {
        this.userId = userId;
        this.userName = userName;
        this.userStatus = userStatus;
        this.password = password;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userStatus='" + userStatus + '\'' +
                '}';
    }
}
