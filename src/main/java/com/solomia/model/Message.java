package com.solomia.model;

public class Message {
    private Integer messageId;
    private String textMessage;
    private Integer receiveFrom;
    private Integer sendTo;
    private Integer diagnosisId;
    private Boolean status;

    public Message() {
    }

    public Message(Integer messageId, String textMessage, Integer receiveFrom, Integer sendTo, Integer diagnosisId, Boolean status) {
        this.messageId = messageId;
        this.textMessage = textMessage;
        this.receiveFrom = receiveFrom;
        this.sendTo = sendTo;
        this.diagnosisId = diagnosisId;
        this.status = status;
    }

    public Integer getMessageId() {
        return messageId;
    }
    
    public String getTextMessage() {
        return textMessage;
    }

    public Integer getReceiveFrom() {
        return receiveFrom;
    }

    public Integer getSendTo() {
        return sendTo;
    }

    public Integer getDiagnosisId() {
        return diagnosisId;
    }

    public Boolean getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Message{" +
                "messageId=" + messageId +
                ", textMessage='" + textMessage + '\'' +
                ", receiveFrom=" + receiveFrom +
                ", sendTo=" + sendTo +
                ", diagnosisId=" + diagnosisId +
                ", status=" + status +
                '}';
    }
}
